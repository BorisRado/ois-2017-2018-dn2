// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika
var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var originalenUkaz = ukaz;
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'barva':
      besede.shift();
      document.querySelector('#sporocila').style.color = besede;
      document.querySelector('#kanal').style.color = besede;
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      var poslji = parametri[1].split(",");
      var youtube = procesirajYoutube(parametri[3]);
      if(parametri){
        for(var i=0; i<poslji.length; i++){
          this.socket.emit('sporocilo', {vzdevek: poslji[i], besedilo: youtube});
        }
       sporocilo = '(zasebno za ' + parametri[1] + '): ' + youtube;
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};

Klepet.prototype.procesirajOmemba = function(sporocilo, trenutniVzdevek){
  var besede = sporocilo.split(' ');
  var posljiOmembo=[];
  for(var i=0; i<besede.length; i++){
    if(besede[i].charAt(0)=='@'){
      posljiOmembo.push(besede[i].substring(1));
    }
  }
  for(i=0; i<posljiOmembo.length; i++){
    if(posljiOmembo[i] != trenutniVzdevek){
      this.socket.emit('sporocilo', {vzdevek: posljiOmembo[i], besedilo:"<span> &#9758; Omemba v klepetu</span>"});
    }
  }
};



var procesirajYoutube = function(sporocilo) {
  var vrni= sporocilo+ " ";
  var besede = sporocilo.split(' ');
  var koliko = 0;
  for(var i=0; i<besede.length; i++){
    if(besede[i].substring(0, 32) =='https://www.youtube.com/watch?v='){
      if(koliko==0) vrni += "<br>";
      var naslov = "https://www.youtube.com/embed/"+besede[i].substring(32);
      vrni += "<iframe src='"+naslov+"' allowfullscreen></iframe> ";
      koliko++;
    } 
  }
  return vrni;
};